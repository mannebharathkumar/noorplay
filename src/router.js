import Vue from "vue";
import Router from "vue-router";
import HomeView from "@/views/HomeView";
import SearchView from "@/views/SearchView";
import MoviesView from "@/views/MoviesView";
import TvShowsView from "@/views/TvShowsView";
import LiveTvView from "@/views/LiveTvView";
import MusicView from "@/views/MusicView";
import WatchListView from "@/views/WatchListView";
import SettingsView from "@/views/SettingsView";

Vue.use(Router);

export default new Router({
  //mode: 'history',
  base: process.env.NODE_ENV === "development" ? "/" : "/noor-play/",
  routes: [
    {
      path: "/",
      redirect: { name: "home" }
    },
    {
      path: "/movies",
      name: "movies",
      component: MoviesView
    },
    {
      path: "/home",
      name: "home",
      component: HomeView
    },
    {
      path: "/search",
      name: "search",
      component: SearchView
    },
    {
      path: "/live",
      name: "live",
      component: LiveTvView
    },
    {
      path: "/music",
      name: "music",
      component: MusicView
    },
    {
      path: "/watchlist",
      name: "watchlist",
      component: WatchListView
    },
    {
      path: "/settings",
      name: "settings",
      component: SettingsView
    },
    {
      path: "/tv-shows",
      name: "tv-shows",
      component: TvShowsView
    },
    {
      path: "/*",
      redirect: { name: "home" }
    }
  ]
});
